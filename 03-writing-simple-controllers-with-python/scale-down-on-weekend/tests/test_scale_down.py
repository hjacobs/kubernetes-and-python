import logging
import time

from pykube import Deployment


def test_web_hello_world(kind_cluster):
    kind_cluster.load_docker_image("scale-down-on-weekend")
    kind_cluster.kubectl("apply", "-f", "rbac.yaml")
    kind_cluster.kubectl("apply", "-f", "deployment.yaml")

    # wait for rolling update
    kind_cluster.kubectl("rollout", "status", "deployment/scale-down-on-weekend")

    # create test nginx deployment
    kind_cluster.kubectl("apply", "-f", "tests/test-nginx.yaml")
    kind_cluster.kubectl("rollout", "status", "deployment/test-nginx")

    assert Deployment.objects(kind_cluster.api).get(name="test-nginx").replicas == 1

    kind_cluster.kubectl(
        "annotate", "deployment/test-nginx", "scale-down-on-weekend=true"
    )

    logging.info("Waiting for scale down to happen..")
    for i in range(10):
        if Deployment.objects(kind_cluster.api).get(name="test-nginx").replicas == 0:
            break
        time.sleep(10)

    assert Deployment.objects(kind_cluster.api).get(name="test-nginx").replicas == 0
