# list-pods.py

Basic example to list Pods on the command line using [pykube-ng](https://pykube.readthedocs.io/).

```
poetry run ./list-pods.py mynamespace
```
