#!/usr/bin/env python3

import pygments
from pygments.lexers import get_lexer_for_filename
from pygments.formatters import HtmlFormatter
from pathlib import Path


formatter = HtmlFormatter()

with open("out.html", "w") as out:

    out.write("<style>")
    out.write("body { font-size: 18px; max-width: 1000px; background: #eee; } ")
    out.write(HtmlFormatter(style="default").get_style_defs())
    out.write("  div pre { padding: 4px; background: #fff; }")
    out.write("</style>")
    out.write("<body>")

    for path in sorted(Path(".").glob("**/*")):
        if path.suffix in (".py", ".yaml") or path.name == "Dockerfile":
            out.write("<div>")
            out.write(f"<h4>{path}</h4>")
            with path.open(encoding="utf-8") as fd:
                contents = fd.read()
                highlighted = pygments.highlight(
                    contents, get_lexer_for_filename(path.name), formatter
                )
                out.write(highlighted)
            out.write("</div>")
