from aiohttp import web


async def hello(request):
    return web.Response(text="Hello, world")


async def health(request):
    return web.Response(text="OK")


app = web.Application()
app.add_routes([web.get("/", hello)])
app.add_routes([web.get("/health", health)])
# disable SIGTERM handling for disruption-free rolling updates
web.run_app(app, handle_signals=False)
