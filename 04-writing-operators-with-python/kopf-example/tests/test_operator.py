import time

from pykube.objects import NamespacedAPIObject


class HelloWorld(NamespacedAPIObject):
    version = "example.org/v1"
    kind = "HelloWorld"
    endpoint = "helloworlds"


def test_web_hello_world(kind_cluster):
    kind_cluster.load_docker_image("kopf-example")
    kind_cluster.kubectl("apply", "-f", "deploy/")

    kind_cluster.kubectl("rollout", "status", "deployment/kopfexample-operator")

    kind_cluster.kubectl("apply", "-f", "example.yaml")

    for i in range(10):
        obj = HelloWorld.objects(kind_cluster.api).get(name="test-1")
        if "status" in obj.obj:
            break
        time.sleep(2)

    assert obj.obj["status"]["on_create"]["message"] == "Hello Prague!"
